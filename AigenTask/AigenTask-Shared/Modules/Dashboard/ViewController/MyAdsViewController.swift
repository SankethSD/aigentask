//
//  MyAdsViewController.swift
//  AigenTask
//
//  Created by Sanketh S D on 12/6/22.
//

import UIKit

class MyAdsViewController: UIViewController {
    
    @IBOutlet weak var noPostView: UIView!
    @IBOutlet weak var myAdsView: UIView!
    
    @IBOutlet weak var myAdsTableView: UITableView!
    
    let userDefaults = UserDefaults.standard
    var curPost: Cars?
    var images: [Data] = []
    var myPosts: [Cars] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        myAdsTableView.delegate = self
        myAdsTableView.dataSource = self
        addRightBarButton()
        viewsLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(userDefaults.object(forKey: UserDefaultsKey.carImages) != nil){
            images = userDefaults.object(forKey: UserDefaultsKey.carImages) as! [Data]
        }
        viewsLoad()
        myAdsTableView.reloadData()
        
    }
    
    func viewsLoad(){
        if(myPosts.count == 0){
            noPostView.isHidden = false
            myAdsView.isHidden = true
        }else{
            noPostView.isHidden = true
            myAdsView.isHidden = false
        }
        myAdsTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let user = UserDetails.fetchUser()
        myPosts = user?.myPosts ?? []
        print("!@!@!!!!!!!!", user?.myPosts)
        viewsLoad()
    }
    
    func addRightBarButton() {
        let button = UIButton()
        button.addTarget(self, action: #selector(createPostClick), for: UIControl.Event.touchUpInside)
        button.setTitle("Create Post", for: .normal)
        button.backgroundColor = UIColor.appColor(.themeOrange)
        button.tintColor = .black
        button.layer.cornerRadius = 8
        let rightButton = UIBarButtonItem(customView: button)
        rightButton.customView?.translatesAutoresizingMaskIntoConstraints = true
        rightButton.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        rightButton.customView?.widthAnchor.constraint(equalToConstant: 120).isActive = true
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func createPostClick() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
        vc.callback = {carData in
            self.curPost = carData
            self.myPosts.append(carData)
            UserDetails.updateUser(key: Constants.myPosts, value: self.myPosts)
            print("!@!@!@!@!@!@!@!@ 1 ", UserDetails.fetchUser()?.myPosts)
            self.myAdsTableView.reloadData()
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension MyAdsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAdsTableViewCell") as! MyAdsTableViewCell
        let post = myPosts[indexPath.row]
        cell.name.text = "\(post.brand ?? "") \(post.model ?? "")"
        cell.price.text = "\(post.price ?? "")"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
