//
//  CreatePostViewController.swift
//  AigenTask
//
//  Created by Sanketh S D on 12/7/22.
//

import UIKit

class CreatePostViewController: UIViewController {
    
    @IBOutlet weak var brandName: UITextField!
    @IBOutlet weak var modelName: UITextField!
    @IBOutlet weak var modelYear: UITextField!
    @IBOutlet weak var totalKmDriven: UITextField!
    @IBOutlet weak var fuelType: UITextField!
    @IBOutlet weak var mileage: UITextField!
    @IBOutlet weak var price: UITextField!
    
    @IBOutlet weak var imagesColView: UICollectionView!
    
    @IBOutlet weak var addImageBtn: UIButton!
    @IBOutlet weak var postMyAdBtn: RoundedButtonWithBorder!
    
    @IBOutlet weak var imagesView: RoundedviewWithBorder!
    
    let imagePicker = UIImagePickerController()
    let userDefaults = UserDefaults.standard
    
    var carImages: [Data] = []
    var carData: Cars?
    
    var callback: ((Cars) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imagesColView.delegate = self
        imagesColView.dataSource = self
        if(carImages.count == 0){
            imagesView.isHidden = true
        }else{
            imagesView.isHidden = false
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(carImages.count == 0){
            imagesView.isHidden = true
        }else{
            imagesView.isHidden = false
        }
        imagesColView.reloadData()
        
    }
    
    
    @IBAction func addImagesBtnClicked(_ sender: Any) {
        
        openImagePicker { imageData, image in
            print("************", imageData)
            self.carImages.append(imageData)
        }
        viewDidLoad()
        imagesColView.reloadData()
        
    }
    
    
    @IBAction func postBtnClicked(_ sender: Any) {
        
        self.userDefaults.set(self.carImages, forKey: UserDefaultsKey.carImages)
        carData = Cars(brand: brandName.text, model: modelName.text, modelYear: modelYear.text, price: price.text, fuelType: fuelType.text, mileage: mileage.text, contactNo: "", image: "", totalKmDriven: totalKmDriven.text)
        
        if(carData != nil){
            callback?(carData!)
            navigationController?.popViewController(animated: true)
        }else{
            showAlert(title: "Error", message: "Error")
        }
        
        
    }
    
    
    
}

extension CreatePostViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return carImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCollectionViewCell", for: indexPath) as! ImagesCollectionViewCell
        
        cell.carImage.image = UIImage(data: carImages[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}
