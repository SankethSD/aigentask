//
//  Constants.swift
//  AigenTask
//
//  Created by Sanketh S D on 12/6/22.
//

import Foundation

struct Constants{
    static let fullName = "fullName"
    static let email = "email"
    static let password = "password"
    static let myPosts = "myPosts"
    
}

struct UserDefaultsKey{
    static let isLoggedIn = "isLoggedIn"
    static let carImages = "carImages"
}

struct Credentials{
    
    static let email = "test@aigen.tech"
    static let password = "AigenTech"
    static let name = "Aigen"
    
}

struct PerformSegue{
    static let loginToDashboard = "LoginToDashboard"
}
