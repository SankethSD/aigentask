//
//  UIViewController+Extensions.swift
//  AigenTask
//
//  Created by Sanketh S D on 12/6/22.
//

import UIKit
import YPImagePicker

extension UIViewController{
    func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func openImagePicker(callback: @escaping(_ imageData: Data, _ image: UIImage) -> Void) {
        let config = configureImagePicker(maximumNumberOfItems: 1, mediaType: .photo)
        let picker = YPImagePicker(configuration: config)
        picker.didFinishPicking { items, cancelled in
            if cancelled {
                print("Picker Cancelled")
                picker.dismiss(animated: true, completion: nil)
                return
            }
            for item in items {
                switch item {
                case .photo(let photo):
                    let imageData = photo.originalImage.jpegData(compressionQuality: 0.7)
//                    updateImageOrientionUpSide()?.jpegData(compressionQuality: 0.7)
                    if imageData != nil {
                        callback(imageData!, photo.originalImage)
                    }
                case .video(let video):
                    print(video)
                }
            }
            picker.dismiss(animated: true, completion: nil)
        }
        present(picker, animated: true, completion: nil)
    }
    
    func configureImagePicker(maximumNumberOfItems: Int, mediaType: YPlibraryMediaType) -> YPImagePickerConfiguration {
        var config = YPImagePickerConfiguration()
        config.isScrollToChangeModesEnabled = true
        config.onlySquareImagesFromCamera = true
        config.usesFrontCamera = true
        config.showsPhotoFilters = false
        config.showsVideoTrimmer = true
        config.shouldSaveNewPicturesToAlbum = true
        config.albumName = "DefaultYPImagePickerAlbumName"
        config.startOnScreen = YPPickerScreen.photo
        config.screens = [.library, .photo, .video]
        config.showsCrop = .none
        config.targetImageSize = YPImageSize.original
        config.overlayView = UIView()
        config.hidesStatusBar = false
        config.hidesBottomBar = false
        config.preferredStatusBarStyle = UIStatusBarStyle.lightContent
        config.maxCameraZoomFactor = 1.0
        config.library.options = nil
        config.library.onlySquare = false
        config.library.isSquareByDefault = true
        config.library.minWidthForItem = nil
        config.library.mediaType = mediaType
        config.library.defaultMultipleSelection = false
        config.library.maxNumberOfItems = maximumNumberOfItems
        config.library.minNumberOfItems = 1
        config.library.numberOfItemsInRow = 4
        config.library.spacingBetweenItems = 1.0
        config.library.skipSelectionsGallery = false
        config.library.preselectedItems = nil
        config.video.trimmerMaxDuration = 900.0
        config.video.libraryTimeLimit = 900.0
        config.video.recordingTimeLimit = 900.0
        return config
    }
}
